﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SolutionSegundoWCFClient.Portal.Startup))]
namespace SolutionSegundoWCFClient.Portal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
