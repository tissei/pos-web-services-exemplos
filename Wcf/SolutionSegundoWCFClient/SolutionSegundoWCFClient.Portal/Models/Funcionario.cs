﻿using System;

namespace SolutionSegundoWCFClient.Portal.Models
{
    public class Funcionario
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public double Salario { get; set; }
        public DateTime DataCadastro { get; set; }
    }
}