﻿using System;
using System.ServiceModel;
using WCF_Funcionario;

namespace SelfHost
{
    class Program
    {
        static void Main(string[] args)
        {
            var host = new ServiceHost(typeof(ServicoFuncionario));
            var endereco = new Uri("http://localhost:8081");
            host.AddServiceEndpoint(typeof(IFuncionario), new BasicHttpBinding(), endereco);
            try
            {
                host.Open();

                Console.WriteLine($"O servico {host.Description.ServiceType} esta rodando.");

                foreach (var endpoint in host.Description.Endpoints)
                {
                    Console.WriteLine(endpoint.Address);
                }

                Console.ReadLine();
                host.Close();
            }
            catch (Exception ex)
            {
                host.Abort();
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }
    }
}
