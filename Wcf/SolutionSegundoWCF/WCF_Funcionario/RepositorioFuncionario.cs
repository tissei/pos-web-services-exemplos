﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCF_Funcionario
{
    public class RepositorioFuncionario
    {
        private static List<Funcionario> listaFuncionario;

        public RepositorioFuncionario()
        {
            listaFuncionario = new List<Funcionario>()
            {
                new Funcionario
                {
                    Id = 1,
                    Nome = "João",
                    Salario = 5000,
                    DataCadastro = DateTime.Now
                },
                new Funcionario
                {
                    Id = 2,
                    Nome = "Maria",
                    Salario = 7000,
                    DataCadastro = DateTime.Now
                }
            };
        }

        public List<Funcionario> ListarFuncionario()
        {
            return listaFuncionario;
        }

        public Funcionario SelecionarFuncionario(int id)
        {
            return listaFuncionario.Find(e => e.Id == id);
        }
    }
}
