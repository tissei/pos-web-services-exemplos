﻿using System.Collections.Generic;
using System.ServiceModel;

namespace WCF_Funcionario
{
    [ServiceContract]
    public interface IFuncionario
    {
        [OperationContract]
        List<Funcionario> ListarFuncionario();

        [OperationContract]
        Funcionario SelecionarFuncionario(int id);
    }
}
