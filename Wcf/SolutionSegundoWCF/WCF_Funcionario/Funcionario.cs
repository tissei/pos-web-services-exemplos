﻿using System;
using System.Runtime.Serialization;

namespace WCF_Funcionario
{
    [DataContract]
    public class Funcionario
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Nome { get; set; }
        [DataMember]
        public double Salario { get; set; }
        [DataMember]
        public DateTime DataCadastro { get; set; }
    }
}
