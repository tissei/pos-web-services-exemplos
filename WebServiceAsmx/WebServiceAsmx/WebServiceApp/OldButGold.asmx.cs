﻿using System.Collections;
using System.Collections.Generic;
using System.Web.Services;

namespace WebServiceApp
{
    /// <summary>
    /// Summary description for OldButGold
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class OldButGold : WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public List<string> Top10Memes()
        {
            return new List<string>
            {
                "Turn down for what",
                "Trololo",
                "Trollface"               
            };
        }
    }
}
