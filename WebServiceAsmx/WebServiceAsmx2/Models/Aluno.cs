﻿namespace Models
{
    public class Aluno
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public int Nota { get; set; } 
    }
}