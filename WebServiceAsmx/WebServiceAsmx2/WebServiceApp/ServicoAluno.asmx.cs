﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Models;

namespace WebServiceApp
{
    /// <summary>
    /// Summary description for ServicoAluno
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ServicoAluno : System.Web.Services.WebService
    {
        private static IEnumerable<Aluno> alunos;

        public ServicoAluno()
        {
            alunos = new List<Aluno>
            {
                new Aluno {Id = 1, Nome = "Tiago Kioshi", Nota = 6},
                new Aluno {Id = 2, Nome = "Zé Gatão", Nota = 100},
                new Aluno {Id = 3, Nome = "Jeferson Bergamzze", Nota = 0}
            };
        }

        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [WebMethod]
        public List<Aluno> Alunos()
        {
            return alunos.ToList();
        }

        [WebMethod]
        public Aluno Detalhes(int id)
        {
            return alunos.FirstOrDefault(a => a.Id == id);
        }
    }
}
