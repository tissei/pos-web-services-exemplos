﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using WebApiSegundo.Models;

namespace WebApiSegundo.Controllers
{
    public class PokemonsController : ApiController
    {
        private static readonly IList<Pokemon> Pokemons = new List<Pokemon>();

        public IList<Pokemon> Get()
        {
            return Pokemons;
        }

        public Pokemon Get(int id)
        {
            return Pokemons.FirstOrDefault(p => p.Id == id);
        }

        public void Post(Pokemon pokemon)
        {
            pokemon.Id = Pokemons.Count + 1;
            Pokemons.Add(pokemon);
        }

        public void Put(Pokemon pokemon)
        {
            Delete(pokemon.Id);
            Pokemons.Add(pokemon);
        }

        public void Delete(int id)
        {
            var pokemon = Get(id);
            Pokemons.Remove(pokemon);
        }
    }
}
