﻿namespace WebApiSegundo.Models
{
    public class Pokemon
    {
        public int Id { get; set; }

        public string Nome { get; set; }
    }
}